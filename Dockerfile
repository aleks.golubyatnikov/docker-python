FROM python:3.7-slim-buster

WORKDIR /app

#RUN groupadd -r python-web-app && useradd -r -m -g python-web-app web-app-user
#USER web-app-user

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY app.py .

ENTRYPOINT ["python3.7", "app.py"]

